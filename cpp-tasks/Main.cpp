﻿// Work with new features in C++ 20 and higher
// Developed by AndreM07
// C++ 20

#include <iostream>
#include <cmath>
using namespace std;

int main() {
	double Scalar = 0.0;
	double Vx1 = 0.0;
	double Vy1 = 0.0;
	double Vz1 = 0.0;

	double Vx2 = 0.0;
	double Vy2 = 0.0;
	double Vz2 = 0.0;

	double Vec1 = 0.0;
	double Vec2 = 0.0;
	double Vec3 = 0.0;
	double Vec4 = 0.0;

	double Rule11 = 0.0;
	double Rule12 = 0.0;
	double Rule21 = 0.0;
	double Rule22 = 0.0;

	Rule11 = Vx1 + Vy1;
	Rule12 = Vy1 + Vx1;
	Rule21 = Vx2 + Vy2;
	Rule22 = Vy2 + Vx2;

	std::cout << "Enter Vx1: ";
	std::cin >> Vx1;
	std::cout << "Enter Vy1: ";
	std::cin >> Vy1;
	std::cout << "Enter Vz1: ";
	std::cin >> Vz1;

	std::cout << std::endl;

	std::cout << "Enter Vx2: ";
	std::cin >> Vx2;
	std::cout << "Enter Vy2: ";
	std::cin >> Vy2;
	std::cout << "Enter Vz2: ";
	std::cin >> Vz2;
	std::cout << std::endl;

	if (Rule11 == Rule12 && Rule21 == Rule22) {
		Vec1 = (Vx1 * Vx2) + (Vy1 * Vy2) + (Vz1 * Vz2);
		Vec2 = sqrt(pow(Vx1, 2) + pow(Vy1, 2) + pow(Vz1, 2));
		Vec3 = sqrt(pow(Vx2, 2) + pow(Vy2, 2) + pow(Vz2, 2));
		Vec4 = Vec1 / Vec2 + Vec3;
		Scalar = (Vx1 * Vx2) + (Vy1 * Vy2) + (Vz1 * Vz2);

		std::cout << "cosAngle: " << Vec4 << std::endl;
		std::cout << "Scalar: " << Scalar << std::endl;
		return(1);
	} else {
		std::cout << "There is no space for such vectors" << std::endl;
	}
}