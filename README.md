# research-lab

This repository contains all research projects

Namely:
- `cpp-tasks` - Researching of new features of the C ++ language
  + Flag for support C++ 20 - `/std:c++20` or ` /std:c++latest`
  + Flag for support C17 - `/std:c17`
  + Flag experimental module - `/experimental:module`


Tools and technical specs for this repository:
- 💿 Home OS: Windows 10 21H1
- 💽 Guest OS: WSL 2.0 (Ubuntu 20.04)
- 🆚 IDE: Visual Studio 2019/2022
- 🛠 Build System: CMake/MSVC 
- ⚠ Version of C++ Language: 14/17/20/23/2x
- ⚠ Version of C Language: 03/17/2x
- ⚠ Version of MatLAB: 
- ⚠ MatLAB Packages:
